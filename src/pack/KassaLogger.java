package pack;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by user on 09.08.2016.
 */

public abstract class KassaLogger {

    private static Logger log = Logger.getLogger("MyLog");
    private static FileHandler fileHandler;

    public static void logInfo(String info) {

        try {
            File folder = new File("Log");
            if (!folder.exists()) {
                folder.mkdir();
            }
            fileHandler = new FileHandler("Log/kassaLog.log", true);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.addHandler(fileHandler);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info(info);
    }
}
