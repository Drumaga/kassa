package pack.products;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

/**
 * Created by user on 07.08.2016.
 */

public class ProductCellCustomizer extends TableCell<Product, String> {

    @Override
    protected void updateItem(String item, boolean empty) {
        HBox hBox = new HBox();
        Label label = new Label(item);
        label.setFont(Font.font("Verdana", 30));
        hBox.getChildren().add(label);
        hBox.setAlignment(Pos.CENTER);
        setGraphic(hBox);
    }
}
