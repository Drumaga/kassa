package pack.products;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import pack.MainForm;
import pack.dbInfoGetter.DBConnector;
import pack.pokupka.Korzina;
import pack.pokupka.KorzinaForm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by user on 05.08.2016.
 */

public class ProductForm extends BorderPane {

    private Button backBtn, korzinaBtn;
    private TableView tableView;
    private static Stage stage;
    private ObservableList buyProducts;

    public ProductForm(String productType, Stage stage) throws IOException {
        this.stage = stage;
        buyProducts = FXCollections.observableArrayList();

        Label mainLable = new Label("Выберите продукты:");
        mainLable.setFont(Font.font("Verdana", 40));
        mainLable.setPadding(new Insets(20, 0, 20, 0));
        backBtn = new Button("Назад к категориям");
        backBtn.setPrefHeight(80);
        backBtn.setFont(Font.font("Verdana", 20));
        BorderPane.setAlignment(backBtn, Pos.CENTER_LEFT);
        korzinaBtn = new Button("Перейти в корзину");
        korzinaBtn.setFont(Font.font("Verdana", 20));
        korzinaBtn.setPrefHeight(80);
        BorderPane.setAlignment(korzinaBtn, Pos.CENTER_RIGHT);
        BorderPane topPane = new BorderPane();
        topPane.setLeft(backBtn);
        topPane.setCenter(mainLable);
        BorderPane.setAlignment(mainLable, Pos.CENTER);
        topPane.setRight(korzinaBtn);
        topPane.setPadding(new Insets(15,15,15,15));
        setTop(topPane);

        tableView = new TableView();
        TableColumn imageCol = new TableColumn("Картинка");
        imageCol.setCellValueFactory(new PropertyValueFactory<Product, Image>("Image"));
        imageCol.setCellFactory(new Callback<TableColumn<Product, Image>, TableCell<Product, Image>>() {
            @Override
            public TableCell<Product, Image> call(TableColumn<Product, Image> param) {
                TableCell<Product, Image> cell = new TableCell<Product, Image>(){
                    @Override
                    protected void updateItem(Image item, boolean empty) {
                        if(item != null){
                            HBox hBox = new HBox();
                            ImageView imageview = new ImageView();
                            imageview.setFitHeight(120);
                            imageview.setFitWidth(120);
                            imageview.setImage(item);
                            hBox.getChildren().add(imageview);
                            hBox.setAlignment(Pos.CENTER);
                            setGraphic(hBox);
                        }
                    }
                };
                return cell;
            }
        });
        TableColumn nameCol = new TableColumn("Наименование");
        nameCol.setCellValueFactory(new PropertyValueFactory<Product, String>("Name"));
        nameCol.setCellFactory(new Callback<TableColumn<Product, String>, TableCell<Product, String>>() {
            @Override
            public TableCell<Product, String> call(TableColumn<Product, String> param) {
                ProductCellCustomizer cell = new ProductCellCustomizer();
                return cell;
            }
        });
        TableColumn countCol = new TableColumn("Количество");
        countCol.setCellFactory(new Callback<TableColumn<Product, String>, TableCell<Product, String>>() {
            @Override
            public TableCell<Product, String> call(TableColumn<Product, String> param) {
                ProductCellCustomizer cell = new ProductCellCustomizer();
                return cell;
            }
        });
        countCol.setCellValueFactory(new PropertyValueFactory<Product, String>("Count"));
        countCol.setCellFactory(new Callback<TableColumn<Product, String>, TableCell<Product, String>>() {
            @Override
            public TableCell<Product, String> call(TableColumn<Product, String> param) {
                ProductCellCustomizer cell = new ProductCellCustomizer();
                return cell;
            }
        });
        TableColumn priceCol = new TableColumn("Цена");
        priceCol.setCellFactory(new Callback<TableColumn<Product, String>, TableCell<Product, String>>() {
            @Override
            public TableCell<Product, String> call(TableColumn<Product, String> param) {
                ProductCellCustomizer cell = new ProductCellCustomizer();
                return cell;
            }
        });
        priceCol.setCellValueFactory(new PropertyValueFactory<Product, String>("Price"));
        tableView.getColumns().addAll(imageCol, nameCol, countCol, priceCol);
        ObservableList<Object> items = FXCollections.observableArrayList();
        Product product = new Product();
        items = product.selectDataFromDatabase(DBConnector.openConnection(), productType);
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setItems(items);
        setCenter(tableView);
        setActionListeners();
    }

    private void setActionListeners() {
        backBtn.setOnAction(event -> {
            stage.close();
            MainForm.showStage();
        });

        korzinaBtn.setOnAction(event -> {
            hideStage();
            putProductToKorzina();
            openKorzinaForm();
        });

        tableView.setRowFactory(tv -> {
            TableRow<Product> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (row.getItem().getCount().equals("кг")) {
                    showAddProductDialog(row);
                }

                if (row.getItem().getCount().equals("шт")) {
                    showPopupMessage(this, true);
                    buyProduct(row, "");
                }
            });
            return row;
        });
    }

    private void openKorzinaForm() {
        Stage stage = new Stage();
        stage.setTitle("Корзина");
        stage.setScene(new Scene(new KorzinaForm(stage, buyProducts), 1024, 768));
        stage.show();
        stage.setFullScreenExitHint("");
        stage.setFullScreen(true);
    }

    private void buyProduct(TableRow<Product> row, String amount) {
        String name = row.getItem().getName();
        String count;
        if (row.getItem().getCount().equals("шт")) {
            count = "1";
        } else {
            count = amount;
        }

        String countName = row.getItem().getCount();
        String price = row.getItem().getPrice();

        if (!count.isEmpty()) {
            productSummator(name, count, countName, price);
        }
    }

    private void productSummator(String name, String count, String countName, String price) {
        Double summa;
        HashMap<String, HashMap<String, String>> tempMap = new HashMap<>();
        if (MainForm.items.containsKey(name)) {
            String sumCount;
            if (countName.equals("шт")) {
                sumCount = String.valueOf(Integer.parseInt(count) + Integer.parseInt(MainForm.items.get(name).get("count")));
            } else {
                sumCount = String.valueOf(Double.parseDouble(count) + Double.parseDouble(MainForm.items.get(name).get("count")));
            }
            summa = Double.parseDouble(sumCount) * Double.parseDouble(price);

            MainForm.items.get(name).replace("count", sumCount);
            MainForm.items.get(name).replace("allPrice", summa.toString());
            tempMap.put(name, MainForm.items.get(name));
            MainForm.items.remove(name);
            MainForm.items.put(name, tempMap.get(name));
        } else {
            HashMap<String, String> subItem = new HashMap<>();
            summa = Double.parseDouble(count) * Double.parseDouble(price);
            subItem.put("count", count);
            subItem.put("countName", countName);
            subItem.put("price", price);
            subItem.put("allPrice", summa.toString());

            MainForm.items.put(name, subItem);
        }
    }

    private void putProductToKorzina() {
        if (MainForm.items.size() > 0) {
            HashMap<String, String> subItem;
            for (Map.Entry entry : MainForm.items.entrySet()) {
                String key = entry.getKey().toString();
                subItem = MainForm.items.get(key);
                Korzina korzina = new Korzina(key, subItem.get("count"), subItem.get("countName"), subItem.get("price"), subItem.get("allPrice"));
                buyProducts.add(korzina);
            }
        }
    }

    private void showAddProductDialog(TableRow<Product> row) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Покупка товара");
        dialog.setHeaderText("Для добавления товара в корзину необходимо указать его вес");
        dialog.setContentText("Введите вес товара:");
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(stage);
        dialog.getEditor().setText("1.000");
        Button ok = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        ok.setText("ОК");
        ok.setPrefSize(100, 50);
        Button cancel = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancel.setText("ОТМЕНА");
        cancel.setPrefSize(100, 50);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !result.get().isEmpty()) {
            if (isValidInput(result.get())) {
                showPopupMessage(this, true);
                buyProduct(row, result.get());
            }
        }
        if (result.isPresent() && result.get().isEmpty()) {
            showPopupMessage(this, false);
        }
    }

    private Boolean isValidInput(String value) {
        try {
            Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return false;
        }
        return (Double.parseDouble(value) != 0.0 && (Double.parseDouble(value) > 0));
    }

    private void showPopupMessage(ProductForm productForm, boolean response) {
        Label productAdded = null;
        if (response) {
            productAdded = new Label("Продукт добавлен в корзину");
            productAdded.setTextFill(Color.web("#50ac3a"));
        }
        if (!response) {
            productAdded = new Label("Не указан вес продукта");
            productAdded.setTextFill(Color.web("#E65B35"));
        }
        productForm.setBottom(productAdded);
        productAdded.setPrefHeight(70);
        productAdded.setFont(Font.font("Verdana", 20));
        ProductForm.setAlignment(productAdded, Pos.CENTER);

        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {}
                return null;
            }
        };
        Label finalProductAdded = productAdded;
        sleeper.setOnSucceeded(event -> productForm.getChildren().remove(finalProductAdded));
        new Thread(sleeper).start();
    }

    public static void hideStage() {
        stage.hide();
        stage.setAlwaysOnTop(false);
    }

    public static void showStage() {
        stage.show();
        stage.setAlwaysOnTop(true);
    }

    public static void closeForm() {
        stage.close();
        stage = null;
    }
}
