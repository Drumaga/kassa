package pack.products;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import pack.dbInfoGetter.DataProvider;


import java.io.IOException;
import java.sql.Connection;

/**
 * Created by user on 06.08.2016.
 */

public class Product extends DataProvider {

    private Image image;
    private String name;
    private String count;
    private String price;

    public Product(){}

    public Product(Image image, String name, String count, String price) {
        this.image = image;
        this.name = name;
        this.count = count;
        this.price = price;
    }

    @Override
    public ObservableList<Object> selectDataFromDatabase(Connection connection, String table) throws IOException {
        return super.selectDataFromDatabase(connection, table);
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
