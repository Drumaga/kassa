package pack.pokupka;

/**
 * Created by user on 07.08.2016.
 */
public class Korzina {

    private String name;
    private String count;
    private String countName;
    private String price;
    private String allPrice;

    public Korzina(String name, String count, String countName, String price, String allPrice) {
        this.name = name;
        this.count = count;
        this.countName = countName;
        this.price = price;
        this.allPrice = allPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCountName() {
        return countName;
    }

    public void setCountName(String countName) {
        this.countName = countName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(String allPrice) {
        this.allPrice = allPrice;
    }
}
