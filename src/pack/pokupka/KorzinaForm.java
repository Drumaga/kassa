package pack.pokupka;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import pack.MainForm;
import pack.products.ProductForm;

import java.sql.Connection;
import java.util.Collections;

/**
 * Created by user on 07.08.2016.
 */
public class KorzinaForm extends BorderPane {

    private Button backBtn, payBtn, clearBtn, deleteBtn;
    private static TableView tableView;
    private static Stage stage;
    private Double itog = 0.0;
    private BorderPane bottonPane;
    private static Label itogLabel;
    private TableColumn allPriceCol;

    public KorzinaForm(Stage stage, ObservableList data) {
        this.stage = stage;
        Collections.reverse(data);

        itogLabel = new Label("Итого:" + " " + "0.0");

        Label mainLable = new Label("Список добавленных товаров:");
        mainLable.setFont(Font.font("Verdana", 40));
        mainLable.setPadding(new Insets(20, 0, 20, 0));

        backBtn = new Button("Назад к категориям");
        backBtn.setPrefHeight(80);
        backBtn.setFont(Font.font("Verdana", 20));
        BorderPane.setAlignment(backBtn, Pos.CENTER_LEFT);
        payBtn = new Button("Оплатить покупки");
        payBtn.setFont(Font.font("Verdana", 20));
        payBtn.setPrefHeight(80);
        BorderPane.setAlignment(payBtn, Pos.CENTER_RIGHT);

        BorderPane topPane = new BorderPane();
        topPane.setLeft(backBtn);
        topPane.setCenter(mainLable);
        BorderPane.setAlignment(mainLable, Pos.CENTER);
        topPane.setRight(payBtn);
        topPane.setPadding(new Insets(15,15,15,15));
        setTop(topPane);

        bottonPane = new BorderPane();
        clearBtn = new Button("Очистить список");
        clearBtn.setPrefHeight(70);
        bottonPane.setLeft(clearBtn);

        deleteBtn = new Button("Удалить продукт");
        deleteBtn.setDisable(true);
        deleteBtn.setPrefHeight(70);
        bottonPane.setCenter(deleteBtn);

        bottonPane.setPadding(new Insets(15,15,15,15));
        setBottom(bottonPane);

        tableView = new TableView();
        TableColumn nameCol = new TableColumn("Наименование");
        nameCol.setCellValueFactory(new PropertyValueFactory<Korzina, String>("Name"));
        nameCol.setCellFactory(new Callback<TableColumn<Korzina, String>, TableCell<Korzina, String>>() {
            @Override
            public TableCell<Korzina, String> call(TableColumn<Korzina, String> param) {
                KorzinaCellCustomizer cell = new KorzinaCellCustomizer();
                return cell;
            }
        });

        TableColumn countCol = new TableColumn("Всего");
        countCol.setCellFactory(new Callback<TableColumn<Korzina, String>, TableCell<Korzina, String>>() {
            @Override
            public TableCell<Korzina, String> call(TableColumn<Korzina, String> param) {
                KorzinaCellCustomizer cell = new KorzinaCellCustomizer();
                return cell;
            }
        });
        countCol.setCellValueFactory(new PropertyValueFactory<Korzina, String>("Count"));

        TableColumn countNameCol = new TableColumn("Количество");
        countNameCol.setCellFactory(new Callback<TableColumn<Korzina, String>, TableCell<Korzina, String>>() {
            @Override
            public TableCell<Korzina, String> call(TableColumn<Korzina, String> param) {
                KorzinaCellCustomizer cell = new KorzinaCellCustomizer();
                return cell;
            }
        });
        countNameCol.setCellValueFactory(new PropertyValueFactory<Korzina, String>("CountName"));

        TableColumn priceCol = new TableColumn("Цена");
        priceCol.setCellFactory(new Callback<TableColumn<Korzina, String>, TableCell<Korzina, String>>() {
            @Override
            public TableCell<Korzina, String> call(TableColumn<Korzina, String> param) {
                KorzinaCellCustomizer cell = new KorzinaCellCustomizer();
                return cell;
            }
        });
        priceCol.setCellValueFactory(new PropertyValueFactory<Korzina, String>("Price"));

        allPriceCol = new TableColumn("Сумма");
        allPriceCol.setCellFactory(new Callback<TableColumn<Korzina, String>, TableCell<Korzina, String>>() {
            @Override
            public TableCell<Korzina, String> call(TableColumn<Korzina, String> param) {
                KorzinaCellCustomizer cell = new KorzinaCellCustomizer();
                return cell;
            }
        });
        allPriceCol.setCellValueFactory(new PropertyValueFactory<Korzina, String>("AllPrice"));

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.getColumns().addAll(nameCol, countCol, countNameCol, priceCol, allPriceCol);
        tableView.setItems(data);

        if (data.size() == 0) {
            setEmptyTableLable();
        }

        setCenter(tableView);
        setActionListeners(data);
        calculateSumm(allPriceCol);
    }

    private void setActionListeners(ObservableList data) {
        backBtn.setOnAction(event -> {
            stage.close();
            tableView.getItems().clear();
            ProductForm.showStage();
        });

        payBtn.setOnAction(event -> {
            showOplataForm();
        });

        clearBtn.setOnAction(event -> {
            clearSpisok();
            setEmptyTableLable();
            payBtn.setDisable(true);
            deleteBtn.setDisable(true);
        });

        tableView.setRowFactory(tv -> {
            TableRow<Korzina> row = new TableRow();
            row.setOnMouseClicked(event -> {
                deleteProduct(row, data);
            });
            return row;
        });

    }

    private void showOplataForm() {
        Stage oplataStage = new Stage();
        oplataStage.setTitle("Оплата товара");
        oplataStage.setAlwaysOnTop(true);
        oplataStage.initOwner(stage);
        oplataStage.initModality(Modality.WINDOW_MODAL);
        oplataStage.setScene(new Scene(new OplataForm(itog, oplataStage, tableView), 800, 500));
        oplataStage.show();
    }

    private void deleteProduct(TableRow<Korzina> row, ObservableList data) {
        deleteBtn.setDisable(false);
        deleteBtn.setOnAction(event -> {
            MainForm.items.remove(row.getItem().getName());
            data.remove(row.getIndex());
            deleteBtn.setDisable(true);
            if (data.size() == 0) {
                setEmptyTableLable();
            }
            itog = 0.0;
            calculateSumm(allPriceCol);
        });
    }

    private static void clearSpisok() {
        tableView.getItems().clear();
        MainForm.items.clear();
        itogLabel.setText("Итого:" + " " + "0.0");
    }

    private static void setEmptyTableLable() {
        Label emptyLabel = new Label("Список покупок пуст");
        emptyLabel.setFont(Font.font("Verdana", 30));
        emptyLabel.setTextFill(Color.web("#00A6DE"));
        tableView.setPlaceholder(emptyLabel);
        itogLabel.setText("Итого:" + " " + "0.0");
    }

    private void calculateSumm(TableColumn allPriceCol) {
        for (int i = 0; i < tableView.getItems().size(); i++) {
            itog += Double.parseDouble(allPriceCol.getCellData(i).toString());
        }
        itogLabel.setText("Итого:" + " " + itog.toString());
        itogLabel.setFont(Font.font("Verdana", 30));
        itogLabel.setTextFill(Color.web("#00A6DE"));
        bottonPane.setRight(itogLabel);
        BorderPane.setAlignment(itogLabel, Pos.CENTER_RIGHT);

        if (itog.equals(0.0)) {
            payBtn.setDisable(true);
        }
    }

    public static void closeForm() {
        clearSpisok();
        setEmptyTableLable();
        stage.close();
        stage = null;
    }
}
