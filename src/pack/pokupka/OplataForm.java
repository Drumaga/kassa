package pack.pokupka;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pack.KassaLogger;
import pack.MainForm;
import pack.products.ProductForm;

import java.util.Optional;

/**
 * Created by user on 09.08.2016.
 */

public class OplataForm extends BorderPane {

    private Button nalBtn, bezNalBtn;
    private Stage stage;
    private Double itog;
    private TableView tableView;

    public OplataForm(Double itog, Stage stage, TableView tableView) {
        this.itog = itog;
        this.stage = stage;
        this.tableView = tableView;

        Label mainLabel = new Label("Выберите способ оплаты:");
        mainLabel.setFont(Font.font("Verdana", 40));
        setTop(mainLabel);
        BorderPane.setAlignment(mainLabel, Pos.CENTER);

        nalBtn = new Button("Наличными");
        nalBtn.setPrefSize(200, 150);
        nalBtn.setFont(Font.font("Verdana", 25));
        bezNalBtn = new Button("Картой");
        bezNalBtn.setPrefSize(200, 150);
        bezNalBtn.setFont(Font.font("Verdana", 25));

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(20);
        gridPane.add(nalBtn, 0, 0);
        gridPane.add(bezNalBtn, 1, 0);
        setCenter(gridPane);
        BorderPane.setMargin(mainLabel, new Insets(60,0,0,0));
        BorderPane.setAlignment(gridPane, Pos.CENTER);

        setActionListeners();
    }

    private void setActionListeners() {
        nalBtn.setOnAction(event -> {
            showPayCountDialog("nal");
        });

        bezNalBtn.setOnAction(event -> {
            showPayCountDialog("bezNal");
        });
    }

    private void showPayCountDialog(String payType){
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Оплата товара");
        if (payType.equals("bezNal")) {
            dialog.setHeaderText("Сумма к оплате:");
            dialog.getEditor().setText(itog.toString());
        } else {
            dialog.setHeaderText("Введите сумму:");
        }
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(stage);
        Button ok = (Button) dialog.getDialogPane().lookupButton(ButtonType.OK);
        ok.setText("РАСЧЁТ");
        ok.setPrefSize(100, 50);
        Button cancel = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        cancel.setText("ОТМЕНА");
        cancel.setPrefSize(100, 50);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !result.get().isEmpty()){
            if (Double.compare(Double.parseDouble(result.get()), itog) >= 0) {
                if (payType.equals("nal")) {
                    if (showSuccessNalPayMessage(Double.parseDouble(result.get()) - itog)) {
                        stage.close();
                        itog = 0.0;
                        MainForm.items.clear();
                        KorzinaForm.closeForm();
                        ProductForm.closeForm();
                        MainForm.showStage();
                    }
                }

                if (payType.equals("bezNal")) {
                    if (showBezNalPayMessage()) {
                        stage.close();
                        itog = 0.0;
                        MainForm.items.clear();
                        KorzinaForm.closeForm();
                        ProductForm.closeForm();
                        MainForm.showStage();
                    }
                }
            }
        }
    }

    private void logBuyProducts(TableView tableView) {
        for (int i = 0; i < tableView.getItems().size(); i++) {
            Korzina item = (Korzina)tableView.getItems().get(i);
            KassaLogger.logInfo(item.getName() + " / " + item.getCount() + " / " + item.getCountName() + " / " + item.getPrice() + " / " + item.getAllPrice());
        }
    }

    private Boolean showSuccessNalPayMessage(Double cashBack) {
        Boolean flag = false;
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Оплата товара");
        alert.setHeaderText(null);
        if (Double.compare(cashBack, 0.0) == 0) {
            alert.setContentText("Товар успешно оплачен!");
            flag = true;
            logBuyProducts(tableView);
            KassaLogger.logInfo("НАЛИЧНЫЙ РАСЧЁТ");
            KassaLogger.logInfo("СУММА ПОКУПОК: " + itog.toString());
            KassaLogger.logInfo("ВВЕДЕНА СУММА: " + String.valueOf(cashBack + itog));
            KassaLogger.logInfo("СДАЧА: " + String.valueOf(cashBack));
            KassaLogger.logInfo("===========================================================");
        }
        if (Double.compare(cashBack, 0.00) > 0) {
            alert.setContentText("Товар успешно оплачен!" + "\n" + "Сдача: " + cashBack.toString());
            flag = true;
            logBuyProducts(tableView);
            KassaLogger.logInfo("НАЛИЧНЫЙ РАСЧЁТ");
            KassaLogger.logInfo("СУММА ПОКУПОК: " + itog.toString());
            KassaLogger.logInfo("ВВЕДЕНА СУММА: " + String.valueOf(cashBack + itog));
            KassaLogger.logInfo("СДАЧА: " + String.valueOf(cashBack));
            KassaLogger.logInfo("===========================================================");
        }
        alert.initOwner(stage);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.showAndWait().isPresent();
        return flag;
    }

    private Boolean showBezNalPayMessage() {
        Boolean flag;
        //////////////////////////////
        Double cartMoney = 200.00; /// Сумма на карте для имитации нехватки средств
        //////////////////////////////
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Оплата товара");
        alert.setHeaderText(null);
        if (Double.compare(cartMoney, itog) >= 0) {
            alert.setContentText("Товар успешно оплачен!");
            flag = true;
            logBuyProducts(tableView);
            KassaLogger.logInfo("БЕЗНАЛИЧНЫЙ РАСЧЁТ");
            KassaLogger.logInfo("СУММА ПОКУПОК: " + itog.toString());
            KassaLogger.logInfo("УСПЕШНАЯ ОПЛАТА");
            KassaLogger.logInfo("===========================================================");
        } else {
            alert.setContentText("Недостаточно средств!");
            flag = false;
            logBuyProducts(tableView);
            KassaLogger.logInfo("БЕЗНАЛИЧНЫЙ РАСЧЁТ");
            KassaLogger.logInfo("СУММА ПОКУПОК: " + itog.toString());
            KassaLogger.logInfo("ОПЛАТА НЕ ПРОИЗОШЛА, НЕДОСТАТОЧНО СРЕДСТВ");
            KassaLogger.logInfo("===========================================================");
        }
        alert.initOwner(stage);
        alert.initModality(Modality.WINDOW_MODAL);
        alert.showAndWait().isPresent();
        return flag;
    }
}
