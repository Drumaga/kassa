package pack.dbInfoGetter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import pack.products.Product;

import javax.swing.*;
import java.io.IOException;
import java.sql.*;

/**
 * Created by user on 06.08.2016.
 */
public abstract class DataProvider {
    private ObservableList<Object> data;
    private final String SELECT_ALL = "SELECT * FROM ";

    public ObservableList<Object> selectDataFromDatabase(Connection connection, String table) throws IOException {
        data = FXCollections.observableArrayList();
        try {
            Statement selectStatement = connection.createStatement();
            ResultSet resultSet = selectStatement.executeQuery(SELECT_ALL + table);
            while (resultSet.next()) {
                Image image = new Image(resultSet.getBinaryStream("picture"));
                String name = resultSet.getString("name");
                String count = resultSet.getString("count");
                String price = resultSet.getString("price");
                Product product = new Product(image, name, count, price);
                data.add(product);
            }
            connection.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Ошибка при загрузке данных");
        }
        return data;
    }
}
