package pack.dbInfoGetter;

import javax.swing.*;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by user on 06.08.2016.
 */
public class DBConnector  {

    public static Connection openConnection() {
        Connection connection = null;
        File dbFile = new File("tovary.db");
        if (dbFile.exists()) {
            try {
                Class.forName("org.sqlite.JDBC");
                connection = DriverManager.getConnection("jdbc:sqlite:tovary.db");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ошибка: файл с базой данных не найден!");
        }
        return connection;
    }
}
