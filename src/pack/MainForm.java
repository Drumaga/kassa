package pack;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import pack.products.ProductForm;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class MainForm extends Application {

    private Button fruktiBtn, ovoshiBtn, krupiBtn, konfetiBtn;
    private static Stage mainStage;
    public static LinkedHashMap<String, HashMap<String, String>> items;

    @Override
    public void start(Stage primaryStage) throws Exception {
        createGUI(primaryStage);
        setActionListeners();
    }

    private void createGUI(Stage primaryStage) {
        items = new LinkedHashMap<>();
        mainStage = primaryStage;

        BorderPane root = new BorderPane();

        Label mainLabel = new Label("Выберите категорию товара:");
        mainLabel.setFont(Font.font("Verdana", 40));
        root.setTop(mainLabel);
        BorderPane.setAlignment(mainLabel, Pos.CENTER);

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(20);
        gridPane.setVgap(20);

        fruktiBtn = new Button("Фрукты");
        fruktiBtn.setFont(Font.font("Verdana", 25));
        fruktiBtn.setPrefSize(200, 150);

        ovoshiBtn = new Button("Овощи");
        ovoshiBtn.setFont(Font.font("Verdana", 25));
        ovoshiBtn.setPrefSize(200, 150);

        krupiBtn = new Button("Крупы");
        krupiBtn.setFont(Font.font("Verdana", 25));
        krupiBtn.setPrefSize(200, 150);

        konfetiBtn = new Button("Конфеты");
        konfetiBtn.setFont(Font.font("Verdana", 25));
        konfetiBtn.setPrefSize(200, 150);

        gridPane.add(fruktiBtn, 0, 0);
        gridPane.add(ovoshiBtn, 1, 0);
        gridPane.add(krupiBtn, 0, 1);
        gridPane.add(konfetiBtn, 1, 1);

        BorderPane.setMargin(mainLabel, new Insets(120,0,0,0));

        root.setCenter(gridPane);
        BorderPane.setAlignment(root, Pos.CENTER);

        primaryStage.setTitle("Покупка товаров");
        primaryStage.setScene(new Scene(root, 1024, 768));
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreen(true);
        primaryStage.show();
    }

    private void setActionListeners() {
        fruktiBtn.setOnAction(event -> {
            openProduktForm("Фрукты", "frukti");
        });

        ovoshiBtn.setOnAction(event -> {
            openProduktForm("Овощи", "ovoshi");
        });

        krupiBtn.setOnAction(event -> {
            openProduktForm("Крупы", "krupi");
        });

        konfetiBtn.setOnAction(event -> {
            openProduktForm("Конфеты", "konfeti");
        });
    }

    private void openProduktForm(String formTitle, String categoty) {
        Stage stage = new Stage();
        try {
            stage.setTitle(formTitle);
            stage.setScene(new Scene(new ProductForm(categoty, stage), 1024, 768));
            stage.setFullScreenExitHint("");
            stage.setFullScreen(true);
            stage.setAlwaysOnTop(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        hideStage();
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public static void hideStage() {
        mainStage.hide();
        mainStage.setAlwaysOnTop(false);
    }

    public static void showStage() {
        mainStage.show();
        mainStage.requestFocus();
        mainStage.setAlwaysOnTop(true);
    }
}
